# Resources

Only for resources that is used in the project to allow attribution and licenses to be defined.

Since resources _often_ are in binary form they should be stored my git-lfs in separate branches. Merge requests containing binaries will be denied since any binaries located in the master branch will be downloaded on _git clone_ operation.

# How To's

## Add a resource

1. Clone the repo: `git clone git@gitlab.com:mocs/resources.git`
2. Create a branch for the resource (*res_<resource>*): `git branch res_logotype && git checkout res_logotype`
3. Make sure that binary is tracked by git-lfs: `git lfs track *.jpg`
4. Create new folder for the resource: `mkdir logotype`
5. Create new files as needed:
    1. README.md
    2. LICENCE.md
    3. ATTRIBUTION.md
    4. etc.
6. Create a new branch for the binary (*res_<resource>_bin*): `git branch res_logotype_bin && git checkout res_logotype_bin`
7. Add the binary to the branch and upload _both_ branches.
8. Create a merge request for the branch _not_ containing the binary!

## Retrieve a resource

1. Clone the repo: `git clone git@gitlab.com:mocs/resources.git`
2. Find the correct branch:
    1. Look in the folders for the desired resource, e.g., _logotype_.
    2. The branch including the binary _should_ be named *res\_<resource>\_bin*.
    3. But double check: `git branch -a`
3. Checkout the branch: `git checkout res_logotype_bin`
