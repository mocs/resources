# container_3D_model resource

This 3D model was downloaded from https://sketchfab.com/3d-models/container-fe8d44ca17e9474bb310478a60aa7a29 @ 2020-02-24.

Credit: Willy Decarpentrie, https://sketchfab.com/skudgee

License: https://creativecommons.org/licenses/by/4.0/
